# VRM_20-21

Assorted projects for VRM class.

Binary artifacts can be found in Releases.

### Project_1

A station with two robot arms in ABB RobotStudio.

### Sem_paper

Tex sources for seminar paper. Compilation using included makefile is recommended. Requires `lualatex`.

Images contained in Sem_paper/images folder are subject to limited use license by IEEE. Terms enclosed.
