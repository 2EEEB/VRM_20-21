\documentclass[a4paper,10pt]{article}

%\usepackage{natbib}
\usepackage{amsthm}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{latexsym}
\usepackage{graphicx}
\usepackage{blindtext}
\usepackage{cite}
\usepackage{caption}

\usepackage{doc}

\newtheorem*{theorem}{Theorem}
\theoremstyle{definition}
\newtheorem*{definition}{Definition}

\hoffset -1in \topmargin 0mm \voffset 0mm \headheight 0mm
\headsep0mm
\oddsidemargin  20mm     %   Left margin on odd-numbered pages.
\evensidemargin 20mm     %   Left margin on even-numbered pages.
\textwidth   170mm       %   Width of text line.
\textheight  252mm

\makeatletter
\renewcommand\@openbib@code{%
     \advance\leftmargin  \z@ %\bibindent
      \itemindent \z@
     % Move bibitems close together
     \parsep -0.8ex
     }
\makeatother

\makeatletter
\renewcommand\section{\@startsection {section}{1}{\z@}%
                                   {-3.5ex \@plus -1ex \@minus -.2ex}%
                                   {1.5ex \@plus.2ex}%
                                   {\large\bfseries}}
\makeatother

\makeatletter
\renewcommand\subsection{\@startsection {subsection}{1}{\z@}%
                                   {-3.5ex \@plus -1ex \@minus -.2ex}%
                                   {1.5ex \@plus.2ex}%
                                   {\normalsize\bfseries}}
\makeatother

\makeatletter
	\setlength{\abovecaptionskip}{3pt}   % 0.25cm 
	\setlength{\belowcaptionskip}{3pt}   % 0.25cm 
\makeatother

\begin{document}
\pagenumbering{arabic}
% \pagestyle{empty}

\begin{center}
{\bf \Large INDUSTRY 4.0 IN CONTEXT OF AGRICULTURE}
\end{center}

\smallskip
\begin{center}
{\large Andrej Pillár}
\end{center}

\smallskip
\begin{center}
Faculty of Mechanical Engineering, Brno University of Technology\\
Institute of Automation and Computer Science\\
Technicka 2896/2, Brno 616 69, Czech Republic\\
192235@vutbr.cz\\
\end{center}

\bigskip
\noindent Abstract: \textit{Industrial advancements offer much improvement outside their original fields as well. Over the years, industrial revolutions transformed every step of the food supply chain. Much like the previous revolutions, the latest one is also poised to bring change to the agricultural sector. The new technologies, however revolutionary,  are not without their challenges though. They face concerns in the areas of security, both of the data produced and the systems themselves, privacy and even culture. There is also a challenge of implementing the necessary infrastructure in rural and often inaccessible environments. This seminar paper provides a brief overview of the current state of agriculture 4.0, possible directions and applications for the new technologies and the challenges they face.}

\vspace*{10pt} \noindent Keywords: \textit{industry 4.0, agriculture, food science, automation, artificial intelligence}

\bigskip
\section{Introduction}
\label{sec:1}
Agriculture has been historically the most prevalent industry in virtually the entire world. It is therefore logical to see improvements from other branches carry over. Ever since the first industrial revolution happened, we see technological advancements support the increasing demand on agricultural systems and, given enough time, help expand them further. The first revolution mechanized agricultural operations, increasing efficiency and enabling operation scaling. The second industrial revolution brought about the widespread use of oil-based fuels. Combined with advancements in transportation technology, it established an extended supply chain opening new markets to farmers. Mass production also inspired a new direction in animal husbandry, moving the trend toward large-scale intensive farming. With the advent of computers and information technology, the third industrial revolution enabled precision agriculture. This recent revolution transformed agriculture through yield monitoring, guided farming systems and variable rate application. Figure \ref{fig:1} provides an overview of the development. The currently happening industrial revolution is no different. The focus on connected systems, edge computing and artificial intelligence could help bring data-driven intelligence to agricultural settings, further improving efficiency and safety of the products. While the uptake and actual implementation of new technologies takes time, resources and new approaches, there is no shortage of research. There are several major ways of translating industry 4.0 into agriculture, each with its own of applications, merits, risks and challenges. \cite{from_ind_to_agr} \cite{smart_agr_surv} \cite{future_of_digi_ag}
\begin{figure}[!htbp]
     \centering
     \includegraphics[scale=0.45]{images/shu1-3003910-large.png}
     \caption{Development of agriculture and the main benefits. \cite{smart_agr_surv} ©2021 IEEE}
     \label{fig:1}
\end{figure}

\section{Industrial agriculture of the present day}
\label{sec:2}
This section explores the currently established patterns and processes in industrialized agriculture. The present epoch, described as agriculture 3.0, is characterized by intensive animal farming and monoculture \cite{from_ind_to_agr}. 
\subsection{Patterns}
\label{ssec:2.1}
Monocultural farming, where a single type of crop is grown each cycle, helped maximize yields and overall production efficiency as it allowed farmers to specialize. The environmental impacts of such a farming pattern are not negligible however. Farming a single crop on the same plot of land depletes soil nutrients excessively and due to the decrease in biodiversity, their replenishment is very slow. This in turn aggravates plant diseases and pests, which have to be controlled, usually by sprayed insecticides and pesticides. Combined with the need for synthetic fertilizers, this practice creates a polluted environment which can be harmful to humans and other life. 
\par Similar problems are faced in the high density animal farming facilities. Conditions in these facilities decrease animal health, and therefore the quality of the final product. The high volume of animal waste also pollutes the surrounding environment. 
\subsection{Processes}
\label{ssec:2.2}
Mechanization is widespread in every step of the process, from land preparation, through planting, to harvesting. Currently used mechanisms are still largely dependent on humans to operate the analog equipment. This, combined with low intelligence in the agricultural sector, greatly hinders prospects of agricultural automation. Progress in the fields of information technology and communication is enabling remote sensing and data gathering in this sector \cite{future_of_digi_ag}. One of the challenges is posed by the rural environment, with its limited signal coverage and overall low available bandwidth. Another challenge for the widespread adoption of sensor networks and sensors in general is their high entry and maintenance cost. This issue is even more sensitive for smaller farming operations.
\subsection{Food supply chain}
\label{ssec:2.3}
The supply chain can be generally divided into five steps: production, processing, distribution, retail and consumption. A model known as business-to-business-to-customer or B2B2C. In this model, food brokers facilitate industrial processing and propagation to wholesalers who in turn supply supermarkets from which the food finally finds its way to the consumers. With the widespread adoption of internet, several new models emerged, such as online-to-offline, customer-to-customer or various forms of social commerce. 
\par The B2B2C model has some significant shortcomings in the areas of food loss and waste. Over 30\% of food gets wasted along the steps of the supply chain according to the Food and Agriculture Organization of the United Nations. These losses are due to lack of technology, improper management or unbalanced supply and demand. The last issue is a symptom of asymmetric information between the suppliers and consumers. This also leads to a lack of transparency and traceability \cite{from_ind_to_agr}.

\section{Development of agriculture 4.0}
\label{sec:3}
The paper by Yang et al. identifies three basic modes of agriculture 4.0 development. The modes are precision agriculture, facility agriculture and order agriculture. These modes can be split by geographical region where the given mode is the most prevalent. Precision agriculture is mostly adopted within the U.S, Western Europe seems to be focusing on order agriculture, while the emerging, indoor based facility agriculture is best represented in Japan. \cite{smart_agr_surv}
\subsection{Precision agriculture}
\label{ssec:3.1}
Precision agriculture is defined by carefully optimizing inputs and processes to maximize efficiency and yield. Planning is supplemented by geographical and positioning data. Crop, land or livestock intelligence gathering is achieved through widespread use of wireless sensor networks (WSN in short), comprised of low-energy, multi-functional devices that collect relevant data. Combination of remote sensing, geographical data, and global positioning data with artificial intelligence methods provides a basis for an expert decision system. Precision agriculture aims to increase efficiency by controlling irrigation and pesticide dosage, in turn minimizing waste and environmental impact. The constant data from sensor networks also enables crop intelligence, helping maximize yields and better utilize available farmland. 
\par The main threat to precision agriculture is the security of sensor networks. Long range sensors in harsh conditions are especially vulnerable to eavesdropping and data manipulation. Another potential threat is sensor location tampering resulting in abnormal data.
\subsection{Facility agriculture}
\label{ssec:3.2}
Facility agriculture aims to provide suitable man-made crop growth environment. Such controlled environment eliminates seasonal and geographical constraints enabling highly scalable and efficient production. This mode shares characteristics with the existing related industries such as some types of animal farming and horticulture. Data gathering in such a closed environment is very convenient and allows fine control over most variables. The main use of facility agriculture lies in providing efficient production against environmental limitations that is highly controllable. It is essentially a combination of industry and agriculture and therefore only suited for highly developed regions. The highly centralized nature of the control systems makes it vulnerable to unauthorized access, same as with any network.
\subsection{Order agriculture}
\label{ssec:3.3}
This mode of development aims to combine the existing technological advancements with a new layer introducing trust, security and traceability to the whole supply chain. Developments in this area would improve transparency of product information, reduce information asymmetry between producers and consumers, and help eliminate imbalance between supply and demand, strengthening food security. These problems are suitable for application of emerging cryptographic technologies such as the blockchain.

\section{Key technologies}
\label{sec:4}
Several of the technologies enabling solutions to the mentioned problems are explored in this section along with their proposed applications and benefits. None of the emerging or established technologies can be viewed as \emph{the} enabler of agriculture 4.0, moving towards this stage requires their innovative application, where they are sometimes even co-dependent. The technologies are presented in no particular order.
\subsection{Internet of Things}
\label{ssec:4.1}
IoT is a core technology of industry 4.0. It is therefore expected to have a leading role in enabling agriculture 4.0 as well. Applications for a connected network of sensors are wide and every agricultural development mode benefits from such technology. The layers of the agricultural network compared to a traditional industrial one are not particularly different. There is a difference in protocols used for communicating with a base station since not all traditionally used protocols are available, especially in rural locations. Additional emphasis is also placed on efficient low power operation \cite{iot_survey}.
\par A particularly interesting paradigm, especially for precision agriculture, is the space–air–ground–undersurface integrated network (SAGUIN). Figure \ref{fig:2} provides and overview of this paradigm. This WSN based platform combines data from sensors at every level. From underground soil networks, underwater networks, through land based autonomous equipment to airborne drone swarms equipped with multispectral imaging capabilities. The network would also utilize capabilities of existing communication and satellite infrastructure, providing a high-coverage, comprehensive data source \cite{from_ind_to_agr}.
\begin{figure}[!htbp]
     \centering
     \includegraphics[scale=0.3]{images/shu2-3003910-large.png}
     \caption{SAGUIN WSN paradigm \cite{smart_agr_surv} ©2021 IEEE}
     \label{fig:2}
\end{figure}
\par IoT in an agricultural setting faces unique challenges from infrastructural and security point of view. Compared to the urban environment of typical industrial applications, the rural locations lack communication infrastructure, with fewer base stations and even areas without cell coverage. Another difference, inherent in the nature of these installations, is their outdoor location. Increased attention therefore needs to be paid to construction of the devices, both from physical security and coverage point of view. This opens up an entirely new segment of IoT devices aimed specifically at deployment in agricultural settings. There are developments in this area but the range of specialist sensors and devices is lacking so far \cite{iot_survey} \cite{from_ind_to_agr}. 
\subsubsection{Sensors}
Besides mentioned specialist sensors, already existing sensing technologies can be easily applied in the field of agricultural analysis. Optical sensors are a good example. Exploiting reflectance properties of plants and land to specific light wavelengths enables multispectral analysis of farmland in general \cite{electroop_sensors}. Existing imaging technologies combined with deep learning enable novel approaches to evaluating farming approaches and supporting decision making with concrete data \cite{biomass_sensing}. Combination of imaging technologies with deep learning has also been successfully applied in evaluating crop health \cite{cropdeep} \cite{strawberry_diseases}.
\subsection{Artificial intelligence methods}
\label{ssec:4.2}
The large amounts of diverse data harvested by sensor networks create a good foundation for employing artificial intelligence. It excels in areas of classification and can be employed as decision support system as well. Most research seems to be in the area of classification based on field captured image data. The possible applications include the already mentioned crop health evaluation but also pest detection or precision chemical application among many others \cite{pest_detection} \cite{tobacco_sprayer}. Image classification through deep learning is of great help for precision mapping and thus strengthening informed decision making. Automated methods for evaluation of farmland both as a whole \cite{citrus_orchards} \cite{biomass_sensing}, and up close \cite{deepl_vine} expedite data acquisition that would otherwise take a significant amount of manpower.
\par There are numerous challenges in adoption of agricultural artificial intelligence. The most obvious being the lack of a universal solution. Differing conditions in various fields, ranging from weather to geography or native pests, can have negative impacts when attempting to utilize a dataset universally. Another problem is the divide between AI researchers and farmers. Multitude of machine learning methods bring results that seem to be applicable. To successfully utilize them and move beyond proofs of concept, both the problems and solutions have to be understood by both sides. It is therefore necessary to form multidisciplinary teams of researchers and agricultural experts to enable efficient application of these methods. As mentioned, machine learning requires huge amounts of data and until that data becomes consistently available, deployment of ML methods is hardly going to achieve results. Development of these methods is therefore tightly tied with adoption of IoT devices and solutions to their respective challenges \cite{from_ind_to_agr} \cite{smart_agr_surv} \cite{iot_survey}. Figure \ref{fig:3} illustrates how artificial intelligence ties together all aspects of agriculture 4.0 development. 
\begin{figure}[!htbp]
     \centering
     \includegraphics[scale=0.4]{images/shu5-3003910-large.png}
     \caption{Far reaching impact of artificial intelligence \cite{smart_agr_surv} ©2021 IEEE}
     \label{fig:3}
\end{figure}
\subsection{Robotics}
\label{ssec:4.3}
Robotisation and unmanned operation are an emerging concept in agriculture. Robotics and task automation are well established in the industrial sector where they brought great improvement to efficiency and productivity. Agricultural application of autonomous systems is heavily tied with artificial intelligence. Some researchers estimate the timeframe for successful implementation of unmanned production to be 50 years \cite{smart_to_unmanned}. Progress varies from sector to sector, with animal operations at the forefront. This is quite logical given the existing structure of animal farming operations is structured very much like the traditional industrial production line \cite{abattoir_robotisation}. Challenges in the other areas include mainly the problem of autonomous orientation especially in the rather harsh and often unpredictable conditions of outdoor plantations or fields \cite{from_ind_to_agr}. The technology is slowly catching up however, and even though it cannot be considered fully autonomous in real conditions just yet, there is a steady improvement \cite{pome_robots}. Research into one of the main driving factors of robot adoption, economics of their deployment, seems to be largely lacking however \cite{robot_economics}. Especially compared to other areas of research into agricultural robotisation. As mentioned in the previous section, there is an abundance of reserach into directly supporting topics, such as yield estimation \cite{vine_yield} and ripeness identification \cite{maturity_learning}. There is also the question of ethics and policy, with some researchers arguing that robotisation of agriculture would bring an even deeper separation from the natural processes and could be, in the case of livestock handling, even considered dangerous \cite{Sparrow2020}. 
\par Considering all of these issues, estimations for widespread adoption of agricultural robots such as presented in \cite{smart_to_unmanned} could be viewed as overly optimistic. Despite the abundance of supporting research, agricultural robotics seems to be greatly hampered by issues of policy, economics and even culture since it is viewed as the biggest contributor to transformation of agriculture into a dehumanized industry \cite{Sparrow2020}.
\subsection{Blockchain}
\label{ssec:4.4}
This cryptographic technology could be utilized to establish trust, transparency and traceability throughout the entire agri-food supply chain. Technology known as smart contracts would provide an immutable transaction records from raw material suppliers all the way to the customer. Blockchain technology could also help secure IoT devices against malicious data manipulation attacks. The technology is by its nature quite complex but if implemented correctly, its benefits are far reaching \cite{agri_blockchain}.
\par Numerous nontrivial challenges prevent meaningful progress in this direction. For the time being, the blockchain is not very scalable, with transactions taking a lot of resources to compute, which is a major obstacle for interoperability with existing transactional networks. The high resource requirement also plays a role, especially since one of the targets of agricultural advancements is also environmental friendliness. Lastly, the technology itself, while providing security to other aspects of the system, can be attacked and misused in various ways \cite{from_ind_to_agr}.

\section{Conclusion}
\label{sec:5}
\begin{figure}[!htbp]
     \centering
     \includegraphics[scale=.55]{images/shu.t3-3003910-large.png}
     \captionof{table}{Summary of technologies driving agriculture 4.0 \cite{smart_agr_surv} ©2021 IEEE}
     \label{fig:4}
\end{figure}

The present patterns of intensive mechanized monocultural farming are still prevalent in modern agriculture. There is however no shortage of research into improving agriculture not only from ecological point of view but also technically, and perhaps most importantly economically. Technologies that enable smart planning and management are on the way towards maturation. Yet more progressive technologies and methods are in active research and development, and though they face often nontrivial challenges as summarized in Table \ref{fig:4}, a big transformation of the entire agricultural system could be technically available within several decades. There are also the questions of policy and ethics, which seem to lack much active debate even though they are sure to be a major issue in adoption of any progressive technologies in agriculture. The existing progress is still largely in the realm of research and academics, with only limited field deployment.


\section*{Disclaimer}
This work is intended for the sole purpose of a seminar paper for internal use by Brno university of technology, IACS. The work does not represent views of the author or associated institution. Figures used under permission by IEEE for academic reuse.
\\
\par \noindent Requirements to be followed when using any portion (e.g., figure, graph, table, or textual material) of an IEEE copyrighted paper in a thesis:
\par 1) In the case of textual material (e.g., using short quotes or referring to the work within these papers) users must give full credit to the original source (author, paper, publication) followed by the IEEE copyright line © 2011 IEEE.
\par 2) In the case of illustrations or tabular material, we require that the copyright line © [Year of original publication] IEEE appear prominently with each reprinted figure and/or table.
\par 3) If a substantial portion of the original paper is to be used, and if you are not the senior author, also obtain the senior author's approval.


% References
%
\begingroup
\makeatletter
\renewcommand\section{\@startsection {section}{1}{\z@}%
                                   {-3.5ex \@plus -1ex \@minus -.2ex}%
                                   {4.5ex \@plus.2ex}%
                                   {\large\bfseries}}
\makeatother

\newpage
\bibliography{references}{}
\bibliographystyle{acm}
\endgroup

\end{document}